﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.Networking;
using System.Web;
using System.Drawing;
using System;
public class WireSegmentationCoordinates : MonoBehaviour
{
    private Vector3 position;
    private string wire_position;

 /// <summary>
 /// Sees where the mouse is clicked on the picture (for the wire tracking) and saves that coordinate the the private variable position
 /// NEED TO CHANGE TO RGB COLOR OF PIXEL INSTEAD
 /// </summary>
     public void Start() {
        // Create a texture. Texture size does not matter, since
        // LoadImage will replace with with incoming image size.
        // WebSocket ws = WebSocketFactory.CreateInstance("ws://127.0.0.1:8765");
        // TcpListener server = new TcpListener(IPAddress.Parse("127.0.0.1"), 8765);
        // server.Start();
        // CallingPython.SessionId = 5;
        // foreach (string file in Directory.EnumerateFiles("/home/siminsights/object_detection_application/object_detection_application/storage/" + CallingPython.SessionId + "/training/images/", "*.png")) {
            byte[] data = System.IO.File.ReadAllBytes("Assets/Images/frame0000.png");
            var tex = new Texture2D(1, 1, TextureFormat.RGB24, false);
            tex.LoadImage(data);
            GetComponent<RawImage>().texture = tex;
        // }
        
        // tex.Apply();
        // byte[] bytes = tex.EncodeToPNG();
        // Destroy(tex);
        // return bytes;
    }
      // void Start()
      // {
      //     StartCoroutine(PostRequest("http://localhost:5000/image_upload/141_2_343", "your json"));
      // }

      // IEnumerator PostRequest(string url, string json)
      // {
      //     var uwr = new UnityWebRequest(url, "POST");
      //     byte[] data = System.IO.File.ReadAllBytes("/home/siminsights/combined_detections/frame0000.png");
      //     // byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);
      //     uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(data);
      //     uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
      //     uwr.SetRequestHeader("Content-Type", "application/json");

      //     //Send the request then wait here until it returns
      //     yield return uwr.SendWebRequest();

      //     if (uwr.isNetworkError)
      //     {
      //         Debug.Log("Error While Sending: " + uwr.error);
      //     }
      //     else
      //     {
      //       Debug.Log("Received: " + uwr.downloadHandler.text);
      //       byte[] jsonToSend = Convert.FromBase64String(uwr.downloadHandler.text);
      //       // byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(uwr.downloadHandler.text);
      //       // byte[] bytes = uwr.downloadHandler.data; 
      //       var tex = new Texture2D(1, 1, TextureFormat.RGB24, false);
      //       tex.LoadImage(jsonToSend);
      //       GetComponent<RawImage>().texture = tex;
      //       // Image.FromStream(ms);
      //      //    Debug.Log("Received: " + uwr.downloadHandler.text);
      //     }
      // }
    void Update()
    {
        RectTransform rt = (RectTransform)this.transform;
       if(Input.GetKey(KeyCode.Mouse0)) {
           if(Input.mousePosition.x >= this.transform.position.x - rt.rect.width/2 && Input.mousePosition.x <= this.transform.position.x + rt.rect.width/2){
                if(Input.mousePosition.y >= this.transform.position.y - rt.rect.height/2 && Input.mousePosition.y <= this.transform.position.y + rt.rect.height/2){
                    position = Input.mousePosition;
		    wire_position = "[" + Input.mousePosition.x.ToString() + ", " + Input.mousePosition.y.ToString() + "]";
                    // UnityEngine.Debug.Log(wire_position);
                }
           }
       }
    }
}
