﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.IO;
using TMPro;
public class VideoUploading : MonoBehaviour
{
    public GameObject[] Allimages;
    public RenderTexture VideoImage;
    public GameObject[] pathPrint;
    private bool image;
 /// <summary>
 /// Only allows for a video upload for real world upload
 /// </summary>
    public void OpenExplorer(){
        string path = EditorUtility.OpenFilePanel("Overwrite with mp4", "", "mp4");
        UnityEngine.Debug.Log(path);
        this.GetComponent<UnityEngine.Video.VideoPlayer>().url = path;
        this.GetComponent<UnityEngine.Video.VideoPlayer>().Play();
        this.GetComponent<UnityEngine.Video.VideoPlayer>().Pause();
    }
/// <summary>
 /// Only allows for a picture upload for synthetic upload
 /// </summary>
    public void OpenExplorer1(){
        string path = EditorUtility.OpenFilePanel("Overwrite with png", "", "*.png;*.jpg");
        pathPrint[0].GetComponent<TMPro.TextMeshProUGUI>().text = path;
    }
    public void DirectoryExplorer(){
        string path = EditorUtility.OpenFilePanel("Path to labels", "", "");
        pathPrint[0].GetComponent<TMPro.TextMeshProUGUI>().text = path;
    }
 /// <summary>
 /// Allows to upload a picture or video for testing and updates the following images/videos
 /// </summary>
    public void OpenExplorer2(){
        string path = EditorUtility.OpenFilePanel("Overwrite with mp4", "", "mp4");
        // if (path.Substring(path.Length - 3) == "mp4"|| path.Substring(path.Length - 3) == "mov"){
        //     this.GetComponent<UnityEngine.Video.VideoPlayer>().url = path;
        //     this.GetComponent<UnityEngine.Video.VideoPlayer>().Play();
        //     this.GetComponent<UnityEngine.Video.VideoPlayer>().Pause();
        //     for(int i = 0; i < Allimages.Length; i++)
        //         Allimages[i].GetComponent<RawImage>().texture = VideoImage;
        // }else{
        //     Texture2D tex = null;
        //     byte[] fileData;
        //     if (System.IO.File.Exists(path))     {
        //         fileData = File.ReadAllBytes(path);
        //         tex = new Texture2D(2, 2);
        //         tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        //          for(int i = 0; i < Allimages.Length; i++)
        //         Allimages[i].GetComponent<RawImage>().texture = tex;
        //         image = true;
        //     }
        // }
        pathPrint[1].GetComponent<TMPro.TextMeshProUGUI>().text = path;
    }
 /// <summary>
 /// Plays the video.
 /// </summary>
    public void Play(){
        UnityEngine.Debug.Log("Called play");
        this.GetComponent<UnityEngine.Video.VideoPlayer>().Play();
    }
 /// <summary>
 /// Sets the startTime to the frame first selected with the wire in the frame by pressing the "Wire in Frame" button in "5 Wire Segmentation Part 1" Screen
 /// </summary>
    private int startTime;
    public List<int> frameSelected = new List<int>();
    public void trackWirePressed(){
        startTime = (int)this.GetComponent<UnityEngine.Video.VideoPlayer>().frame;
    }
 /// <summary>
 /// Adds all the frames to the frameSelected List for which frames have the wire in it by having the end be when the "Wire out of Frame" is pressed in the same screen above.
 /// NEED TO PASS THE FRAMESELECTED LIST AS AN ARGUMENT FOR THE PYTHON SCRIPT SEE FUNCTION IN CALLINGPYTHON.CS 
 /// </summary>
    public void stopTrackWirePressed(){
        int endTime = (int)this.GetComponent<UnityEngine.Video.VideoPlayer>().frame;
        for(int i = startTime; i < endTime; i++){
              frameSelected.Add(i);
        }
        Debug.Log(frameSelected.Count);
    }
 /// <summary>
 /// Pauses the video
 /// </summary>
    public void Pause(){
        this.GetComponent<UnityEngine.Video.VideoPlayer>().Pause();
    }

 /// <summary>
 /// Rotates the image and the following images after that clockwise 90 degrees.
 /// </summary>
    public void Rotate90(){
        for(int i = 0; i < Allimages.Length; i++)
            Allimages[i].transform.Rotate (Vector3.forward * -90);
    }

 /// <summary>
 /// Rotates the image and the following images after that clockwise 270 degrees.
 /// </summary>
    public void Rotate270(){
        for(int i = 0; i < Allimages.Length; i++)
            Allimages[i].transform.Rotate (Vector3.forward * 90);
    }
 /// <summary>
 /// Flips the image and the following images after that vertically.
 /// </summary>
    public void flip(){
        for(int i = 0; i < Allimages.Length; i++)
            Allimages[i].transform.Rotate (Vector3.up * 180);
    }
}
