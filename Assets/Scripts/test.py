import socket
import struct
import traceback
import logging
import time
import numpy as np
import cv2
import asyncio
import websockets
from os import path
import os
import argparse
import socket

def sending_and_reciveing():
    s = socket.socket()
    socket.setdefaulttimeout(None)
    print('socket created ')
    port = 8765
    s.bind(('127.0.0.1', port)) #local host
    s.listen(30) #listening for connection for 30 sec?
    print('socket listensing ... ')
    while True:
        try:
            c, addr = s.accept() #when port connected
            bytes_received = c.recv(4000) #received bytes
            sess_id = bytes_received.decode("utf-8")  #converting into float array
            print("Received!", sess_id)

            if not path.exists(local_storage + "/" + str(sess_id)):
            	msg = "Invalid session ID!"
            	bytes_to_send = str.encode(msg)
            	# await websocket.send(msg)
            	# bytes_to_send = str.encode(nn_output) #converting float to byte
            	c.sendall(bytes_to_send) #sending back
            	c.close()
            else:
            	training_folder = local_storage + "/" + str(sess_id) + "/training/"
            	if not os.path.exists(training_folder + "labels"):
            		os.makedirs(training_folder + "labels")
            	msg = "Created labels folder"
            	bytes_to_send = str.encode(msg)
            	# await websocket.send(msg)
            	# bytes_to_send = str.encode(nn_output) #converting float to byte
            	c.sendall(bytes_to_send) #sending back

            	bytes_received = c.recv(4000) #received bytes
            	label_name = bytes_received.decode("utf-8") 
            	print("Label name recieved ", label_name)
            	c.close()
            # nn_output = "returned array" #NN prediction (e.g. model.predict())

            # bytes_to_send = str.encode(nn_output) #converting float to byte
            # c.sendall(bytes_to_send) #sending back
            # c.close()
        except Exception as e:
            logging.error(traceback.format_exc())
            print("error")
            c.sendall(bytearray([]))
            c.close()
            break


# async def hello(websocket, paths):
# 	sess_id = await websocket.recv()
# 	print(f"< {sess_id}")
# 	if not path.exists(local_storage + "/" + str(sess_id)):
# 		msg = "Invalid session ID!"
# 		await websocket.send(msg)
# 	else:
# 		training_folder = local_storage + "/" + str(sess_id) + "/training/"
# 		if not os.path.exists(training_folder + "labels"):
# 			os.makedirs(training_folder + "labels")
# 		msg = "Created labels folder"
# 		await websocket.send(msg)

# 		label_name = await websocket.recv()
# 		print(f"< {label_name}")

# 		count = await websocket.recv()

# 		# first bounding box on first frame (0,0,0,0) if object not in first frame
# 		bb = await websocket.recv()
# 		bbsplit = bb.split(",")

# 		start_point = (int(bbsplit[0]), int(bbsplit[1]))
# 		end_point = (int(bbsplit[2]), int(bbsplit[3]))

# 		if start_point == (0, 0) and end_point == (0, 0):
# 			stop_tracking = "True"
# 			print("No object in first frame!")

# 		tracker = cv2.TrackerKCF_create()

# 		if (start_point[0] > end_point[0]) or (start_point[1] > end_point[1]):
# 			temp_x = start_point[0]
# 			temp_y = start_point[1]
# 			start_point = end_point
# 			end_point = (temp_x, temp_y)

# 		bbox = (start_point[0], start_point[1], end_point[0] - start_point[0], end_point[1] - start_point[1])

# 		cnt = 0


# 		while cnt < int(count):
# 			cnt = cnt + 1
# 			img_name = await websocket.recv()
# 			bb = await websocket.recv()
# 			stop_tracking = await websocket.recv()
# 			update_track = await websocket.recv()

# 			bbsplit = bb.split(",")

# 			start_point = (int(bbsplit[0]), int(bbsplit[1]))
# 			end_point = (int(bbsplit[2]), int(bbsplit[3]))
# 			if update_track == "True":
# 				print("Bounding box updated!!")
# 				tracker = cv2.TrackerKCF_create()
# 				bbox = (
# 				start_point[0], start_point[1], end_point[0] - start_point[0], end_point[1] - start_point[1])
# 				img = cv2.imread(training_folder + '/images/' + img_name + '.png')
# 				ok = tracker.init(img, bbox)
# 			if stop_tracking == "True":
# 				img = cv2.imread(training_folder + '/images/' + img_name + '.png')
# 				kitti_label = ''
# 				bbox_to_send = '0, 0, 0, 0'
# 			else:
# 				img = cv2.imread(training_folder + '/images/' + img_name + '.png')
# 				pr_bbox = bbox
# 				ok, bbox = tracker.update(img)

# 				if ok:
# 					# Tracking success
# 					if (int(bbox[0]) < 0):
# 						bbox = (0, bbox[1], bbox[2], bbox[3])
# 					if (int(bbox[1]) < 0):
# 						bbox = (bbox[0], 0, bbox[2], bbox[3])
# 				else:
# 					# Tracking failure
# 					bbox = pr_bbox
# 				bbox_to_send = str(bbox[0]) + ' ' + str(bbox[1]) + ' ' + str(
# 					bbox[0] + bbox[2]) + ' ' + str(bbox[1] + bbox[3])
# 				kitti_label = label_name + ' 0.0 0 0.0 ' + str(bbox[0]) + ' ' + bbox_to_send + ' 0.0 0.0 0.0 0.0 0.0 0.0 0.0\n'

# 			f = open(training_folder + "labels/" + img_name + ".txt", "a+")
# 			f.write(kitti_label)
# 			f.close()
# 			await websocket.send(bbox_to_send)

# 	print(f"> complete")
# 	await websocket.send("complete")


parser = argparse.ArgumentParser(description='Run annotation generation on training video')
parser.add_argument('storage_path', type=str, help='Location of local storage')
args = parser.parse_args()
local_storage = args.storage_path  # local_storage = "/home/siminsights/object_detection_application/storage"

sending_and_reciveing() 

# start_server = websockets.serve(hello, "127.0.0.1", 8765)

# asyncio.get_event_loop().run_until_complete(start_server)
# asyncio.get_event_loop().run_forever()

