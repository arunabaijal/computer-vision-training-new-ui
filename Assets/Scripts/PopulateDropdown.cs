using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using TMPro;

public class PopulateDropdown : MonoBehaviour
{
    public TMP_Dropdown TMPDropdown;
    private TMP_Text text;
 /// <summary>
 /// Populates the dropdown with all the session in the file directory
 /// </summary>
    public void populateSessionDropDown(){
        int SessionCount = System.IO.Directory.GetDirectories(@"/home/siminsights/object_detection_application/object_detection_application/storage").Length;
        UnityEngine.Debug.Log(SessionCount);
        TMPDropdown.options.Clear ();
        for(int i= 1; i <= SessionCount; i++)
        {
            string t = i.ToString();
            TMPDropdown.options.Add (new TMP_Dropdown.OptionData() {text="Session " + t});
        }
    }
    private void Start() {
        populateSessionDropDown();
    }
   
}
