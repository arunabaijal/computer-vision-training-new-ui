using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using TMPro;

public class PopulateDropdownInference : MonoBehaviour
{
    public TMP_Dropdown TMPDropdown;
    private TMP_Text text;
    public GameObject allNames;
 /// <summary>
 /// Populates the dropdown with all the session in the file directory
 /// </summary>
    public void populateSessionDropDown(){
        string labels = allNames.GetComponent<TMPro.TextMeshProUGUI>().text;
        string[] objects = labels.Split(',');
        TMPDropdown.options.Clear ();
        foreach (var obj in objects)
        {
            TMPDropdown.options.Add (new TMP_Dropdown.OptionData() {text=obj.ToString()});
        }
    }
    private void Start() {
        populateSessionDropDown();
    }
   
}
