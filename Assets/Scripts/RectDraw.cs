﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

using HybridWebSocket;

public class RectDraw : MonoBehaviour
{
   private Vector2 _box_start_pos = Vector2.zero;
   private Vector2 _box_end_pos = Vector2.zero;
   public Texture SelectionTexture;
   public bool finalBox = false;
   public static bool showNext = false;
   public static string[] images;
   public static int count = 0;
   public static string bbox = "(0,0,0,0)";
   public static int totalCount;
   public static bool updateTrack = false;
   public static bool stopTrack = false;
    public void Start() {
        // Create a texture. Texture size does not matter, since
        // LoadImage will replace with with incoming image size.
        // WebSocket ws = WebSocketFactory.CreateInstance("ws://127.0.0.1:8765");
        // TcpListener server = new TcpListener(IPAddress.Parse("127.0.0.1"), 8765);
        // server.Start();
        // CallingPython.SessionId = 5;
        // foreach (string file in Directory.EnumerateFiles("/home/siminsights/object_detection_application/object_detection_application/storage/" + CallingPython.SessionId + "/training/images/", "*.png")) {
        CallingPython.SessionId = 16;
        string path = "Assets/Images/frame0000.png";
        // if (Directory.Exists(path)) {
        //     images = Directory.GetFiles(path, @"*.png", SearchOption.TopDirectoryOnly);
        // }
        // totalCount = images.Length;
        byte[] data = System.IO.File.ReadAllBytes(path);
        // count = count + 1;
        var tex = new Texture2D(1, 1, TextureFormat.RGB24, false);
        tex.LoadImage(data);
        GetComponent<RawImage>().texture = tex;
        // }
        
        // tex.Apply();
        // byte[] bytes = tex.EncodeToPNG();
        // Destroy(tex);
        // return bytes;
    }
 /// <summary>
 /// Draws a rectangle for selection by following the mouse. If you just click then the rectangle is removed
 /// </summary>
 void Update ()
 {
     // Called while the user is holding the mouse down.
     RectTransform rt = (RectTransform)this.transform;  
     if(Input.GetKey(KeyCode.Mouse0))
     {
         //Checks if it is within the picture
         if(Input.mousePosition.x >= this.transform.position.x - rt.rect.width/2 && Input.mousePosition.x <= this.transform.position.x + rt.rect.width/2){
                if(Input.mousePosition.y >= this.transform.position.y - rt.rect.height/2 && Input.mousePosition.y <= this.transform.position.y + rt.rect.height/2){
                    stopTrack = false;
                    showNext = false;
                    // Called on the first update where the user has pressed the mouse button.
                    if (Input.GetKeyDown(KeyCode.Mouse0)) {
                        _box_start_pos = Input.mousePosition;
                        // UnityEngine.Debug.Log("start position " + _box_start_pos.ToString());
                    }
                    else { // Else we must be in "drag" mode.
                        _box_end_pos = Input.mousePosition;  
                        // UnityEngine.Debug.Log("end position " + _box_end_pos.ToString());
                        } 

                    Vector2 result;
                    Vector2 clickPosition = Input.mousePosition;
                    RectTransform thisRect = GetComponent<RectTransform>();

                    RectTransformUtility.ScreenPointToLocalPointInRectangle(thisRect, clickPosition, null, out result);
                    result += thisRect.sizeDelta / 2;

                    // UnityEngine.Debug.Log("Position " + result.ToString());
                    // bbox = "(" + _box_start_pos.x.ToString() + "," + _box_start_pos.y.ToString() + "," + _box_end_pos.x.ToString() + "," + _box_end_pos.y.ToString() + ")";
                    // showNext = true;
                    // updateTrack = true;

                }
             } 
     }
     // if (Input.GetKey(KeyCode.Mouse1))
     // {
     //    stopTrack = true;
     // }
     // if(showNext && count < totalCount) {
     //    UnityEngine.Debug.Log("Next image");
     //    byte[] data = System.IO.File.ReadAllBytes(images[count]);
     //    var tex = new Texture2D(1, 1, TextureFormat.RGB24, false);
     //    tex.LoadImage(data);
     //    GetComponent<RawImage>().texture = tex;
     //    char[] charsToTrim = { '(', ')'};
     //    string bbox_trimmed = bbox.Trim(charsToTrim);
     //    string[] points = bbox_trimmed.Split(',');
     //    _box_start_pos.x = float.Parse(points[0]);
     //    _box_start_pos.y = float.Parse(points[1]);
     //    _box_end_pos.x = float.Parse(points[2]);
     //    _box_end_pos.y = float.Parse(points[3]);
     //    count = count + 1;
     //    showNext = false;
     //    updateTrack = false;

     // }
 }
 /// <summary>
 /// Draws the selection rectangle if the user is holding the mouse down.
 /// </summary>
 void OnGUI()
 {
     // If we are in the middle of a selection draw the texture.
     if(_box_start_pos != Vector2.zero && _box_end_pos != Vector2.zero)
     {
         // Create a rectangle object out of the start and end position while transforming it
         // to the screen's cordinates.
         var rect = new Rect(_box_start_pos.x, Screen.height - _box_start_pos.y,
                             _box_end_pos.x - _box_start_pos.x,
                             -1 * (_box_end_pos.y - _box_start_pos.y));
        // UnityEngine.Debug.Log("transformed position " + _box_start_pos.x.ToString() + " " + (Screen.height - _box_start_pos.y).ToString() + " " + (_box_end_pos.x - _box_start_pos.x).ToString() + " " + 
        //                      (-1 * (_box_end_pos.y - _box_start_pos.y)).ToString());
         // Draw the texture.
         GUI.DrawTexture(rect, SelectionTexture);
     }
 }
}
