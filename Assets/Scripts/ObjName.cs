﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ObjName : MonoBehaviour
{
    public GameObject inputField;
    public GameObject allNames;
    public GameObject instructions;
    public GameObject name;
    public GameObject error;
    public GameObject button;
    public GameObject tempNextBtn;
    public static string text = "";
    public static string newRect = "";
    private List<string> objectLabels = new List<string>();

 /// <summary>
 /// Gets the new label name of the object wanting to be labeled
 /// Error checks the label to see if it is filled and then adds it to a a list of all the labesl called "objectLabels"
 /// </summary>
    public void Inputedname()
    {
        text = inputField.GetComponent<TMP_InputField>().text;
        if(text != ""){
            name.GetComponent<TMPro.TextMeshProUGUI>().text = text;
            objectLabels.Add(text);
            inputField.GetComponent<TMP_InputField>().text = "";

            // CallingPython.SessionId = 5;
            // UnityEngine.Debug.Log("Sending Session ID");
            // string prediction = ServerRequest(CallingPython.SessionId.ToString());
            // UnityEngine.Debug.Log(prediction);


            // UnityEngine.Debug.Log("Sending label name");
            // prediction = ServerRequest(ObjName.text);
            // UnityEngine.Debug.Log(prediction);

            // UnityEngine.Debug.Log("Sending total count");
            // prediction = ServerRequest(RectDraw.totalCount.ToString());
            // UnityEngine.Debug.Log(prediction);

            // UnityEngine.Debug.Log("Sending first bbox");
            // prediction = ServerRequest(RectDraw.bbox);
            // UnityEngine.Debug.Log(prediction);

            startVideo();
            Allnames();
        }
        else{
             error.SetActive(true);
        }
    }
 
 /// <summary>
 ///  Moves on the the next set of instructions to track the video throughout the video so hides, unhides the correspondign fields
 /// </summary>
    void startVideo(){
        instructions.GetComponent<TMPro.TextMeshProUGUI>().text = "As the object is tracked through the video, edit the bounding box if needed by redrawing the bounding box around the object with mouse. To remove bounding box when object is out of frame, click the mouse.";
        inputField.SetActive(false);
        name.SetActive(true);
        button.SetActive(false);
        tempNextBtn.SetActive(true);
        // string prediction;
        // while (RectDraw.count < RectDraw.totalCount) {
        //     UnityEngine.Debug.Log("Sending image name");
        //     prediction = ServerRequest(RectDraw.images[RectDraw.count]);
        //     UnityEngine.Debug.Log(prediction);
        //     UnityEngine.Debug.Log("Sending bbox");
        //     prediction = ServerRequest(RectDraw.bbox);
        //     UnityEngine.Debug.Log(prediction);
        //     UnityEngine.Debug.Log("Sending stop tracker");
        //     prediction = ServerRequest(RectDraw.stopTrack.ToString());
        //     UnityEngine.Debug.Log(prediction);
        //     UnityEngine.Debug.Log("Sending update tracker");
        //     prediction = ServerRequest(RectDraw.updateTrack.ToString());
        //     UnityEngine.Debug.Log(prediction);
        //     RectDraw.showNext = true;
        // }

    }

 /// <summary>
 ///  Resets all the components to the start so that another object can be labeled
 /// </summary>
    public void resetforNewLabel(){
        instructions.GetComponent<TMPro.TextMeshProUGUI>().text ="Draw bounding box around object by dragging your mouse. Press enter when satisfied with the bounding box. Make sure the bounding box is snug around the object and the object is entirely inside the box.";
        inputField.SetActive(true);
        name.SetActive(false);
        button.SetActive(true);
        tempNextBtn.SetActive(false);
    }
    
 /// <summary>
 ///  Updates what should be listed for the label names on the training parameters page
 /// </summary>
    void Allnames(){
        string final = ""; 
        for(int i = 0; i < objectLabels.Count; i++){
            final += objectLabels[i];
            if(i != objectLabels.Count - 1){
                final += ", ";
            }
        }
        allNames.GetComponent<TMPro.TextMeshProUGUI>().text = final;
        
    }
}
