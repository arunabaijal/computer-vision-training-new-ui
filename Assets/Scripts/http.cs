using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.Networking;
using System.Web;  
using System.IO;  
using System.Drawing;  

public class http : MonoBehaviour {
	void Start()
	{
	    StartCoroutine(PostRequest("http://localhost:5000/image_upload/141_2_343", "your json"));
	}

	IEnumerator PostRequest(string url, string json)
	{
	    var uwr = new UnityWebRequest(url, "POST");
	    byte[] data = System.IO.File.ReadAllBytes("/home/siminsights/Pictures/inference_cvtrainapp.png");
	    // byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);
	    uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(data);
	    uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
	    uwr.SetRequestHeader("Content-Type", "application/json");

	    //Send the request then wait here until it returns
	    yield return uwr.SendWebRequest();

	    if (uwr.isNetworkError)
	    {
	        Debug.Log("Error While Sending: " + uwr.error);
	    }
	    else
	    {
	    	byte[] bytes = uwr.downloadHandler.data; 
	    	var tex = new Texture2D(1, 1, TextureFormat.RGB24, false);
            tex.LoadImage(bytes);
            GetComponent<RawImage>().texture = tex;
	    	// Image.FromStream(ms);
	     //    Debug.Log("Received: " + uwr.downloadHandler.text);
	    }
	}
}
