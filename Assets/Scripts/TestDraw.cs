﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using HybridWebSocket;

public class TestDraw : MonoBehaviour
{
   private Vector2 _box_start_pos = Vector2.zero;
   private Vector2 _box_end_pos = Vector2.zero;
   public Texture SelectionTexture;
   public bool finalBox = false;
    public void Start() {
        // Create a texture. Texture size does not matter, since
        // LoadImage will replace with with incoming image size.
        // WebSocket ws = WebSocketFactory.CreateInstance("ws://127.0.0.1:8765");
        // TcpListener server = new TcpListener(IPAddress.Parse("127.0.0.1"), 8765);
        // server.Start();
        // CallingPython.SessionId = 5;
        // foreach (string file in Directory.EnumerateFiles("/home/siminsights/object_detection_application/object_detection_application/storage/" + CallingPython.SessionId + "/training/images/", "*.png")) {
            byte[] data = System.IO.File.ReadAllBytes("Assets/Images/frame0000.png");
            var tex = new Texture2D(1, 1, TextureFormat.RGB24, false);
            tex.LoadImage(data);
            GetComponent<RawImage>().texture = tex;
        // }
        
        // tex.Apply();
        // byte[] bytes = tex.EncodeToPNG();
        // Destroy(tex);
        // return bytes;
    }
}
