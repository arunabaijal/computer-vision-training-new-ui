﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using TMPro;
using System.Net.Sockets;
using System.Net;

public class CallingPython : MonoBehaviour
{
    public static int SessionId; 
    public int frame;
    private int rotation = 4;
    private string training_video_path;
    public GameObject allNames;
    public GameObject epochs;
    public GameObject batch_size;
    // private static CallingPython instance = null;
    // public static CallingPython SharedInstance {
    //     get {
    //         if (instance == null) {
    //             UnityEngine.Debug.Log("Creating new instace of CallingPython");
    //             instance = new CallingPython ();
    //         }
    //         return instance;
    //     }
    // }
    // private string wire_position;

 /// <summary>
 /// Calls the corresponding python script for the realWorldData screen 
 /// Need to change the arg and cmd path according to your computer 
 /// </summary>  

    public void OpenExplorer(){
        training_video_path = EditorUtility.OpenFilePanel("Overwrite with mp4", "", "mp4");
        UnityEngine.Debug.Log(training_video_path);
    }

    public void realWorldData(){
        ProcessStartInfo start = new ProcessStartInfo();
        string args = "/home/siminsights/object_detection_application/object_detection_application/startTrainingSession.py /home/siminsights/object_detection_application/object_detection_application/storage " + training_video_path;
        string cmd = "python3";
        start.FileName = cmd;//cmd is full path to python.exe
        start.Arguments = args;//args is path to .py file and any cmd line args
        start.UseShellExecute = false;
        start.RedirectStandardOutput = true;
        using(Process process = Process.Start(start))
        {
            using(StreamReader reader = process.StandardOutput)
            {
                string result = reader.ReadToEnd();
                UnityEngine.Debug.Log(result);
                // Console.Write(result);
            }
        }
        SessionId = System.IO.Directory.GetDirectories(@"/home/siminsights/object_detection_application/object_detection_application/storage").Length;
        UnityEngine.Debug.Log("Session ID is: " + SessionId);
    }

 /// <summary>
 /// Calls the corresponding python script for the ObjectLabelingStart screen 
 /// Need to change the arg and cmd path according to your computer 
 /// </summary> 
    public void ObjectLabelingStart(){
        ProcessStartInfo start = new ProcessStartInfo();
        string args = "/home/siminsights/object_detection_application/object_detection_application/generateLabelsWithTracker.py /home/siminsights/object_detection_application/object_detection_application/storage";
        string cmd = "python3";
        start.FileName = cmd;//cmd is full path to python.exe
        start.Arguments = args;//args is path to .py file and any cmd line args
        start.UseShellExecute = false;
        start.RedirectStandardOutput = true;
        using(Process process = Process.Start(start))
        {
            using(StreamReader reader = process.StandardOutput)
            {
                string result = reader.ReadToEnd();
                UnityEngine.Debug.Log(result);
            }
        }
    }

 /// <summary>
 /// Calls the corresponding python script for the startTraining screen 
 /// Need to change the arg and cmd path according to your computer 
 /// </summary> 
    public void startTraining(){
        ProcessStartInfo start = new ProcessStartInfo();
        string labels = allNames.GetComponent<TMPro.TextMeshProUGUI>().text;
        string epoch = epochs.GetComponent<TMPro.TextMeshProUGUI>().text;
        string batchsize = batch_size.GetComponent<TMPro.TextMeshProUGUI>().text;
        string args = "/home/siminsights/object_detection_application/object_detection_application/generateSpecs.py /home/siminsights/object_detection_application/object_detection_application/storage" + SessionId.ToString() + " " + labels + " " + batchsize + " " + epoch;
        string cmd = "python3";
        start.FileName = cmd;//cmd is full path to python.exe
        start.Arguments = args;//args is path to .py file and any cmd line args
        start.UseShellExecute = false;
        start.RedirectStandardOutput = true;
        using(Process process = Process.Start(start))
        {
            using(StreamReader reader = process.StandardOutput)
            {
                string result = reader.ReadToEnd();
                UnityEngine.Debug.Log(result);
            }
        }
        // int directoryCount = System.IO.Directory.GetDirectories(@"/home/siminsights/object_detection_application/object_detection_application/storage").Length;
        args = "/home/siminsights/object_detection_application/object_detection_application/isaacDockerRunner.py " + SessionId.ToString() + " 1";
        cmd = "python3";
        start.FileName = cmd;//cmd is full path to python.exe
        start.Arguments = args;//args is path to .py file and any cmd line args
        start.UseShellExecute = false;
        start.RedirectStandardOutput = true;
        using(Process process = Process.Start(start))
        {
            using(StreamReader reader = process.StandardOutput)
            {
                string result = reader.ReadToEnd();
                UnityEngine.Debug.Log(result);
            }
        }
        // int directoryCount = System.IO.Directory.GetDirectories(@"/home/siminsights/object_detection_application/object_detection_application/storage").Length;
        args = "/home/siminsights/object_detection_application/object_detection_application/isaacDockerRunner.py " + SessionId.ToString() + " 2";
        cmd = "python3";
        start.FileName = cmd;//cmd is full path to python.exe
        start.Arguments = args;//args is path to .py file and any cmd line args
        start.UseShellExecute = false;
        start.RedirectStandardOutput = true;
        using(Process process = Process.Start(start))
        {
            using(StreamReader reader = process.StandardOutput)
            {
                string result = reader.ReadToEnd();
                UnityEngine.Debug.Log(result);
            }
        }
    }
 
 /// <summary>
 /// Calls the python script for the startTesting screen 
 /// Need to change the arg and cmd path according to your computer SessionId is the id chosen from the dropdown
 /// </summary> 
    public TMP_Dropdown TMPDropdown;
    public void startTesting(){
        ProcessStartInfo start = new ProcessStartInfo();
        UnityEngine.Debug.Log(TMPDropdown.value);
        SessionId = TMPDropdown.value + 1;
        rotation = 4;
        string args = "/home/siminsights/object_detection_application/object_detection_application/runInference.py /home/siminsights/object_detection_application/object_detection_application/storage "+ SessionId.ToString() + " /home/siminsights/Videos/breaker_1.mp4";
        string cmd = "python3";
        start.FileName = cmd;//cmd is full path to python.exe
        start.Arguments = args;//args is path to .py file and any cmd line args
        start.UseShellExecute = false;
        start.RedirectStandardOutput = true;
        using(Process process = Process.Start(start))
        {
            using(StreamReader reader = process.StandardOutput)
            {
                string result = reader.ReadToEnd();
                UnityEngine.Debug.Log(result);
            }
        }
    }

    /// <summary>
    /// Correct orientation for testing images
    /// </summary>
    public void rotateClockwise(){
        rotation = 1;
    }

    public void rotateAnticlockwise(){
        rotation = 3;
    }

    public void flipImages(){
        rotation = 2;
    }

    public void correctOrientation(){
        ProcessStartInfo start = new ProcessStartInfo();
        // UnityEngine.Debug.Log(TMPDropdown.value);
        // SessionId = TMPDropdown.value + 1;
        string args = "/home/siminsights/object_detection_application/object_detection_application/runInference.py /home/siminsights/object_detection_application/object_detection_application/storage "+ SessionId.ToString() + " /home/siminsights/Videos/breaker_1.mp4" + rotation.ToString();
        string cmd = "python3";
        start.FileName = cmd;//cmd is full path to python.exe
        start.Arguments = args;//args is path to .py file and any cmd line args
        start.UseShellExecute = false;
        start.RedirectStandardOutput = true;
        using(Process process = Process.Start(start))
        {
            using(StreamReader reader = process.StandardOutput)
            {
                string result = reader.ReadToEnd();
                UnityEngine.Debug.Log(result);
            }
        }
    }

    // public void Image_Click(object sender, MouseEventArgs e)
    // {
    //     int xCoordinate = e.X;
    //     int yCoordinate = e.Y;
    //     UnityEngine.Debug.Log(xCoordinate.ToString() + " " + yCoordinate.ToString());
    // }

    // public void Update()
    // {
    //     RectTransform rt = (RectTransform)this.transform;
    //    if(Input.GetKey(KeyCode.Mouse0)) {
    //        if(Input.mousePosition.x >= this.transform.position.x - rt.rect.width/2 && Input.mousePosition.x <= this.transform.position.x + rt.rect.width/2){
    //             if(Input.mousePosition.y >= this.transform.position.y - rt.rect.height/2 && Input.mousePosition.y <= this.transform.position.y + rt.rect.height/2){
    //                 wire_position = "[" + Input.mousePosition.x.ToString() + ", " + Input.mousePosition.y.ToString() + "]";
    //                 UnityEngine.Debug.Log(wire_position);
    //             }
    //        }
    //    }
    // }
    
    /// <summary>
    /// Calls the python script for the tackWire screen 
    /// Need to change the arg and cmd path according to your computer SessionId is the id chosen from the dropdown
    ///Incomplete the args is not completed see Transition Documentation
    /// </summary> 
    public void trackWire(){
        ProcessStartInfo start = new ProcessStartInfo();
        string args = "";//getWireSegement.py
        string cmd = "python3";
        start.FileName = cmd;//cmd is full path to python.exe
        start.Arguments = args;//args is path to .py file and any cmd line args
        start.UseShellExecute = false;
        start.RedirectStandardOutput = true;
        using(Process process = Process.Start(start))
        {
            using(StreamReader reader = process.StandardOutput)
            {
                string result = reader.ReadToEnd();
                UnityEngine.Debug.Log(result);
            }
        }
    }
}
