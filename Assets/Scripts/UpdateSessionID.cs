using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using TMPro;

public class UpdateSessionID : MonoBehaviour
{
    TextMeshProUGUI mText;
    void Start() {
        mText = gameObject.GetComponent<TextMeshProUGUI>();
        mText.text = CallingPython.SessionId.ToString();
    }
   
}
