﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
 /// <summary>
 /// Enables the corresponding screens when next is pressed
 /// </summary>
    public GameObject[] menuObjects;
    public void SwapToMenu(int menuId)
    {
        foreach(GameObject menu in menuObjects)
        {
            menu.SetActive(false);
        }
        menuObjects[menuId].SetActive(true);
    }
}
